package binTree;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Stack;

public class MyTree<K, V> {

  private transient int size;
  private final Comparator<? super K> comparator = setComparator();
  private Entry<K, V> root;


  private Comparator<? super K> setComparator() {
    return Comparator.comparing(Object::toString);
  }


  public void put(K key, V value) {
    Entry<K, V> t = root;
    if (t == null) {

      root = new Entry<>(key, value, null);
      size = 1;
      return;
    }

    Entry<K, V> entry = this.findExistingEntry(key);
    if (entry != null) {
      this.changeValue(entry, value);
    } else {
      this.putNew(key, value);
    }


  }

  public V get(K key) {
    Entry<K, V> entry = this.findExistingEntry(key);
    if (entry != null) {
      return entry.value;
    } else {
      throw new NoSuchElementException();
    }

  }

  public V remove(K key) {
    Entry<K, V> existing = this.findExistingEntry(key);
    if (existing == null) {
      throw new NoSuchElementException("Element is not found");
    }
    if (existing.parent != null) {
      Entry<K, V> parent = existing.parent;
      if (existing.haveNoChildren()) {
        if (parent.left == existing) {
          parent.deleteLeftChild();
        }
        if (parent.right == existing) {
          parent.deleteRightChild();
        }
      } else {

        if (!existing.canAddRightChild && existing.canAddLeftChild) {
          existing = existing.right;
          existing.parent = parent;
        } else if (!existing.canAddLeftChild && existing.canAddRightChild) {
          existing = existing.left;
          existing.parent = parent;
        } else if (!existing.canAddAnyChildren()) {
          this.swapChild(existing, parent);

        }
      }

    } else {
      this.swapChild(existing, null);
    }

    return existing.getValue();
  }

  public void print() {
    Map<Integer, String> info = new HashMap<>();
    Stack<Entry<K, V>> stack = new Stack<>();
    stack.add(this.root);
    while (!stack.isEmpty()) {
      Entry<K, V> current = stack.pop();
      info.put(current.level,
          current.key.toString() + ", " + current.value.toString() + ", " + current.parent.getKey()
              .toString());
      if (!current.canAddLeftChild) {
        stack.add(current.left);

      }
      if (!current.canAddRightChild) {
        stack.add(current.right);
      }
    }

    for (Map.Entry<Integer, String> entry : info.entrySet()) {
      System.out.println(entry.getKey() + ", " + entry.getValue());
    }


  }


  private void putNew(K key, V value) {
    Stack<Entry<K, V>> stack = new Stack<>();
    stack.add(this.root);
    while (!stack.isEmpty()) {
      Entry<K, V> current = stack.pop();
      if (current.canAddAnyChildren()) {
        Entry<K, V> child = new Entry<>(key, value, current);
        if (current.canAddRightChild) {
          current.right = child;
          return;
        }
        if (current.canAddLeftChild) {
          current.left = child;
          return;
        }
      } else {
        if (!current.canAddLeftChild) {
          stack.add(current.left);
        }
        if (!current.canAddRightChild) {
          stack.add(current.right);
        }
      }


    }
  }

  private void swapChild(Entry<K, V> current, Entry<K, V> parent){
    Entry<K, V> child;
    Entry<K, V> newRight;
    child = current.right;
    current = current.left;
    current.parent = parent;
    newRight = current.right;
    while(!newRight.canAddRightChild){
      newRight = newRight.right;
    }
    newRight.right = child;
  }

  private void changeValue(Entry<K, V> entry, V value) {
    entry.value = value;
  }

  private Entry<K, V> findExistingEntry(K key) {
    Stack<Entry<K, V>> stack = new Stack<>();
    stack.add(this.root);
    while (!stack.isEmpty()) {
      Entry<K, V> current = stack.pop();
      if (current.key.equals(key)) {
        return current;
      } else {
        if (!current.canAddLeftChild) {
          stack.add(current.left);
        }
        if (!current.canAddRightChild) {
          stack.add(current.right);
        }
      }
    }
    return null;
  }


  private static final class Entry<K, V> implements Map.Entry<K, V> {

    int level;
    K key;
    V value;
    Entry<K, V> left;
    Entry<K, V> right;
    Entry<K, V> parent;
    boolean canAddLeftChild = true;
    boolean canAddRightChild = true;

    Entry(K key, V value, Entry<K, V> parent) {
      this.key = key;
      this.value = value;
      this.parent = parent;
      if (this.parent != null) {
        this.level = this.parent.level + 1;
      } else {
        level = 1;
      }
    }

    boolean canAddAnyChildren() {
      if (canAddRightChild || canAddLeftChild) {
        return true;
      }
      return false;
    }

    boolean haveNoChildren() {
      if(!canAddLeftChild && !canAddRightChild){
        return true;
      }
      return false;
    }

    void deleteLeftChild() {
      this.right = null;
      this.canAddLeftChild = true;
    }

    void deleteRightChild() {
      this.left = null;
      this.canAddRightChild = true;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }

    public V setValue(V value) {
      V oldValue = this.value;
      this.value = value;
      return oldValue;
    }


  }

}

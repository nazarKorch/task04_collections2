package menu;

@FunctionalInterface
public interface Printable {
  public void print();

}

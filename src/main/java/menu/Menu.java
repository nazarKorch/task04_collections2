package menu;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

  private Map<String, String> menuToShow;
  private Map<String, Printable> menu;
  private Scanner scanner = new Scanner(System.in);

  public Menu() {
    menuInit();
  }

  private void menuInit() {
    menuToShow = new HashMap<>();
    menuToShow.put("1", "1 - show products");
    menuToShow.put("2", "2 - languages");
    menuToShow.put("Q", "Q - exit");

    menu = new HashMap<>();
    menu.put("1", this::showProducts);
    menu.put("2", this::showLaguages);

  }

  private void showMenu() {
    for (String s : menuToShow.values()) {
      System.out.println(s);
    }
  }

  public void start() {
    String input;

    do {
      showMenu();
      input = scanner.nextLine();
      try {
        menu.get(input).print();

      } catch (Exception e) {
        System.out.println("wrong number");
      }

    } while (!input.equalsIgnoreCase("Q"));
  }


  private void showProducts() {
    System.out.println("products showed");
  }

  private void showLaguages() {
    System.out.println("languages showed");
  }


}
